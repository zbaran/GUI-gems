Authors: Zofia Baranczuk (GUI), zosia.baranczuk@gmail.com, Luisa Salazar Vizcaya, Nello Blaser, Thomas Gsponer (gems)

A GUI for R package gems.

To install:

Download file GUIgems0.1.tar.gz 

install.packages(path_to_GUIgems0.1.tar.gz, repos = NULL, type="source")

Documentation in GUIgems.pdf
